
# The application demonstrates working with streaming data.
Reading data from the kafka topic and displaying the data in the browser.

# Archtecture
The application consists of two parts:

1) Data is retrieved from the source and stored in storage. This stage is necessary in order to save historical data, so that you can, for example, compare them with the current ones.

   1) Data comes from source

      The source can be a third-party service to which we connect via API and read out the data,
      and a self-written service that generates data (this option is implemented) depending
      from the selected strategy for the AppConfiguration.ratesConsumer bean.

   2) The application saves data
      
      Data can be sent both to the database and to streaming (for example, Kafka or PubSub or something else).
      An interface has been prepared for sending data
      The choice of strategy depends on the bean
      This application implements sending data to Kafka.  

## Server
This is the part of the application that does the following:
1) reads data from source
2) adds the subtracted data to a Kafka topic
3) reads data from the topic topic and sends it to the web socket.

## Client
It is the part of the application that displays data in the browser.
1) Using the API, requests data from the database
2) Connects to the server via a web socket and receives data in streaming mode


# Launching the application
1. Run docker-compose up -d
2. Wait for the application to launch.
3. Open your browser and go to http://localhost:8080
4. Choose data source : database or kafka
5. Chosen page
   1. If you chose database then click button "Get data from database" and data will be displayed in the table
   2. If you chose kafka then please button "Start" and data will be refreshing every time when there is new data coming from source in to a kafka topic
   


