package com.example.rates.service;

import com.example.rates.database.PostgresService;
import com.example.rates.model.RateDTO;
import com.example.rates.serde.JsonToRateDTODeserializer;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;


@RunWith(MockitoJUnitRunner.class)
class RateSnapshotWrapperTest {

    @Test
    public void testWrap1() throws SQLException {
        JsonToRateDTODeserializer deserializer = new JsonToRateDTODeserializer();

        RateDTO rateDTO = new RateDTO("EURUSD", 1.08);
        rateDTO.setRateTS(1713726000L);
        List<RateDTO> rateDTOList = List.of(rateDTO);

        RateDTO expectedRateDTO = new RateDTO("EURUSD", 1.07, -0.010000000000000009, 1713726000L);
        expectedRateDTO.setRateTS(1713611466187L);
        

        RatesService ratesService = Mockito.mock(RatesService.class);
        Mockito.when(ratesService.getRatesOn5PMNY()).thenReturn(rateDTOList);

        RateSnapshotWrapper rateSnapshotWrapper = new RateSnapshotWrapper("16:59:29", "17:00:01", ratesService);
        String result = rateSnapshotWrapper.wrap("{\"rate\":1.07,\"currencyCouple\":\"EURUSD\",\"rateTS\":1713611466187}");

        RateDTO rateDTOWrapped = deserializer.deserialize(result);

        //compare result

        // assert(result.equals(expected));
        assert(expectedRateDTO.equals(rateDTOWrapped));
    }

    @Test
    public void testWrap2() throws SQLException {
        JsonToRateDTODeserializer deserializer = new JsonToRateDTODeserializer();

        RateDTO rateDTO = new RateDTO("EURUSD", 1.08);
        rateDTO.setRateTS(1713646800L); // 20.04.24 17:00 PM Ney York
        List<RateDTO> rateDTOList = List.of(rateDTO);

        RateDTO expectedRateDTO = new RateDTO("EURUSD", 1.07, 0.0, 1713733200000L);
        expectedRateDTO.setRateTS(1713733200000L);

        RatesService ratesService = Mockito.mock(RatesService.class);
        Mockito.when(ratesService.getRatesOn5PMNY()).thenReturn(rateDTOList);

        RateSnapshotWrapper rateSnapshotWrapper = new RateSnapshotWrapper("16:59:29", "17:00:01", ratesService);
        String result = rateSnapshotWrapper.wrap("{\"rate\":1.07,\"currencyCouple\":\"EURUSD\",\"rateTS\":1713733200000}");
        RateDTO rateDTOWrapped = deserializer.deserialize(result);

        //compare result
        assert(expectedRateDTO.equals(rateDTOWrapped));
    }


}