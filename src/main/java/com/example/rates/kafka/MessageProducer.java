package com.example.rates.kafka;

import com.example.rates.AProducer;
import com.example.rates.service.IProducer;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

@Slf4j
@Setter
public class MessageProducer extends AProducer {

    private KafkaProducer<String, String> kafkaProducer;

    private String topicName;

    private String bootstrapServers;

    private String acks;

    public MessageProducer(String bootstrapServers, String topicName, String acks) throws UnknownHostException {
        this.bootstrapServers = bootstrapServers;
        this.topicName = topicName;
        this.acks = acks;

        // Create a Kafka producer with the given properties
        Properties properties = new Properties();
        properties.put(ProducerConfig.CLIENT_ID_CONFIG, InetAddress.getLocalHost().getHostName());
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(ProducerConfig.ACKS_CONFIG, acks);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        kafkaProducer = new KafkaProducer<String, String>(properties);
    }

    @Override
    public void produce(String message) {

        final ProducerRecord<String, String> record = new ProducerRecord<>(topicName, null, message);
        kafkaProducer.send(record, new Callback() {
            public void onCompletion(RecordMetadata metadata, Exception e) {
                if (e != null)
                    log.debug("Send failed for record {}", record, e);
            }
        });

        log.info("Sent message: " + message);
    }

}
