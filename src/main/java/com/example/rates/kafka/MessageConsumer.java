package com.example.rates.kafka;

import com.example.rates.AConsumer;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;

@Slf4j
@Setter
public final class MessageConsumer extends AConsumer {

    private Properties properties;

    private String topicName;
    private String groupId;
    private String bootstrapServers;
    private String offsetResetConfig;
    private int queueCapacity;

    public MessageConsumer(String bootstrapServers, String topicName, String groupId, String offsetResetConfig, int queueCapacity) {
        queue = new ArrayBlockingQueue<>(queueCapacity);
        this.queueCapacity = queueCapacity;

        this.topicName = topicName;
        this.groupId = groupId;
        this.bootstrapServers = bootstrapServers;
        this.offsetResetConfig = offsetResetConfig;

        if (offsetResetConfig == null) {
            this.offsetResetConfig = "latest";
        }
    }

    @Override
    public void consume() {
    }

    public void run() {
        KafkaConsumer<String, String> consumer = getKafkaConsumer();

        log.info("Start consuming messages...");
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<String, String> record : records) {
                addToQueue(record.value());
                log.info("Key: " + record.key() + ", Value:" + record.value());
            }
        }
    }


    private KafkaConsumer<String, String> getKafkaConsumer() {
        //Creating consumer properties
        properties = new Properties();
        properties.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        properties.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, offsetResetConfig);

        //creating consumer
        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(properties);
        //Subscribing
        consumer.subscribe(Arrays.asList(topicName));
        return consumer;
    }

}
