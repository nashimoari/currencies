package com.example.rates.model;

import lombok.Data;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;

/**
 * It generates exchange rates for 5 pairs of currencies
 */
@Data
@Service
public class Rates {

    private Map<String, Double> rates = new HashMap<>();

    private String[] currencyPairs;

    // Methods
    public void printRates() {
        for (String currencyPair : rates.keySet()) {
            System.out.println(currencyPair + ": " + rates.get(currencyPair));
        }
    }

}
