package com.example.rates.model;

import lombok.Data;

@Data
public class RateDTO {
    private String currencyCouple; // e.g. "EURUSD"
    private Double rate; // e.g. 1.05
    private Long rateTS; // Timestamp when rate was fixed
    private Double historyRateDiff; // e.g. 0.99
    private Long historyRateDiffTS; // Timestamp when history rate was fixed

    public RateDTO(String currencyCouple, Double rate) {
        this.currencyCouple = currencyCouple;
        this.rate = rate;
        this.rateTS = System.currentTimeMillis();
    }

    public RateDTO(String currencyCouple, Double rate, Double historyRateDiff, Long historyRateDiffTS) {
        this.currencyCouple = currencyCouple;
        this.rate = rate;
        this.rateTS = System.currentTimeMillis();
        this.historyRateDiff = historyRateDiff;
        this.historyRateDiffTS = historyRateDiffTS;
    }
}
