package com.example.rates.model;

public class RatesMessage {

    private String pair;
    private Double value;

    public RatesMessage() {
    }

    public RatesMessage(String pair, Double value) {
        this.pair = pair;
        this.value = value;
    }

    public String getContent() {
        return pair + ":" + value;
    }

}