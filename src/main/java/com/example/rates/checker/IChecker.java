package com.example.rates.checker;

import com.example.rates.serde.IDeserializer;

public interface IChecker {
    boolean check(String message, IDeserializer deserializer);
}
