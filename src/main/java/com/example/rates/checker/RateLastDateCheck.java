package com.example.rates.checker;

import com.example.rates.model.RateDTO;
import com.example.rates.serde.IDeserializer;

import java.util.HashMap;
import java.util.Map;

public class RateLastDateCheck implements IChecker {
    private Map<String, Long> latestRates;

    public RateLastDateCheck() {
        latestRates = new HashMap<>();
    }

    @Override
    public boolean check(String message, IDeserializer deserializer) {
        RateDTO rate = (RateDTO) deserializer.deserialize(message);
        if (!latestRates.containsKey(rate.getCurrencyCouple())) {
            latestRates.put(rate.getCurrencyCouple(), rate.getRateTS());
            return true;
        }

        return latestRates.get(rate.getCurrencyCouple()) <= rate.getRateTS();
    }
}
