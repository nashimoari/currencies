package com.example.rates.api.controller;

import com.example.rates.model.RateDTO;
import com.example.rates.service.RatesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/rates")
public class RatesController {

    @Autowired
    private RatesService ratesService;


    @GetMapping(produces = "application/json")
    public String getRates() throws SQLException {
        log.info("Get rates from database");

        // Get rates from database
        List<RateDTO> rates = ratesService.getRates();

        // Convert rates to JSON string
        String ratesJson = ratesService.convertRatesToJson(rates);

        // Return rates as JSON string
        log.info(ratesJson);

        return ratesJson;
    }
}
