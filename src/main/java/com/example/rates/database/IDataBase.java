package com.example.rates.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;


public interface IDataBase {

    public Connection connect() throws SQLException, InterruptedException;

    boolean execute(String sql, List<SqlParam> params);

    public ResultSet getData(String sql, List<SqlParam> params) throws SQLException;


}
