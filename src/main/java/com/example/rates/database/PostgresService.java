package com.example.rates.database;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class PostgresService implements IDataBase {

    @Value("${db-meta.jdbc-string}")
    private String url;

    @Value("${db-meta.login}")
    private String login;

    @Value("${db-meta.password}")
    private String password;


    /**
     * Connect to the PostgreSQL database
     *
     * @return a Connection object
     */
    public Connection connect() throws SQLException {
        int errorCount = 0;

        while (true) {
            try {
                return DriverManager.getConnection(url, login, password);
            } catch (Exception e) {
                if (errorCount >= 10)
                    throw new RuntimeException("Error getting connection to database after 10 attempts. Message: " + e.getMessage());
                errorCount++;
                log.error("Try number " + errorCount + ":Error getting connection to database: " + e.getMessage());
                log.info("JDBC connection string: " + url);
                log.info("JDBC login: " + login);
                log.info("JDBC password: " + password);
                log.info("Waiting 5 seconds before next attempt...");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }

    public ResultSet getData(String sql, List<SqlParam> params) {
        ResultSet rs = null;
        try {
            Connection conn = connect();
            PreparedStatement stmt = conn.prepareStatement(sql);
            applyParameters(stmt, params);

            rs = stmt.executeQuery();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return rs;
    }


    public boolean execute(String sql, List<SqlParam> params) {
        boolean result = false;
        try {
            Connection conn = connect();
            PreparedStatement stmt = conn.prepareStatement(sql);
            applyParameters(stmt, params);

            result = stmt.execute();
        } catch (SQLException ex) {
            log.error(ex.getMessage());
        }
        return result;
    }

    private void applyParameters(PreparedStatement stmt, List<SqlParam> params) throws SQLException {
        if (params == null) return;

        String key;
        for (int idx = 0; idx < params.size(); idx++) {
            key = params.get(idx).getType();
            switch (key) {
                case ("integer"):
                    stmt.setInt(idx + 1, (Integer) params.get(idx).getValue());
                    break;

                case ("long"):
                    stmt.setLong(idx + 1, (Long) params.get(idx).getValue());
                    break;

                case ("double"):
                    stmt.setDouble(idx + 1, (Double) params.get(idx).getValue());
                    break;

                case ("string"):
                    stmt.setString(idx + 1, (String) params.get(idx).getValue());
                    break;

                case ("timestamp"):
                    stmt.setTimestamp(idx + 1, new Timestamp((Long) params.get(idx).getValue()));
                    break;

                default:
                    throw new SQLException("Unknown parameter type");
            }
        }
    }

}
