package com.example.rates.database;

import com.example.rates.AProducer;
import com.example.rates.mapper.IMapper;
import com.example.rates.serde.IDeserializer;
import com.example.rates.service.IProducer;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Produce data to database
 */
@Setter
@Slf4j
public class DBProducer extends AProducer {

    BlockingQueue<String> queue;

    String sqlQuery;

    IDataBase dataBase;

    IDeserializer deserializer;

    IMapper mapper;

    @Override
    public void produce(String message) {

    }

    @Override
    public void run() {
        String message;
        while (true) {
            log.info("Waiting for data from queue...");
            // Wait for data in consumer queue
            try {
                message = queue.poll(30, TimeUnit.SECONDS);
                log.info(message);

                // Deserialize data to object
                Object object = deserializer.deserialize(message);
                if (object == null) {
                    log.error("Error deserializing data");
                    continue;
                }
                log.info(object.toString());

                // Map object to SqlParam
                List<SqlParam> sqlParams = mapper.mapOne(object);

                try {
                    // Insert data to database
                    dataBase.execute(sqlQuery, sqlParams);
                } catch (Exception e) {
                    log.error("Error inserting data into database", e);
                }

                log.info("Data inserted into database");

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            if (message != null) {
                produce(message);
            }

        }
    }
}
