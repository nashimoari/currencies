package com.example.rates.mapper;

import com.example.rates.database.SqlParam;
import com.example.rates.model.RateDTO;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class RateToSqlParam implements IMapper {

    @Override
    public List<List<SqlParam>> map(Object objClass) {
        throw new RuntimeException("Not implemented");
    }

    public List<SqlParam> mapOne(Object objClass) {
        List<SqlParam> item = new ArrayList<>();
        if (objClass == null) {
            return item;
        }

        RateDTO rate = (RateDTO) objClass;

        log.info("Mapping rateDTO to SQL parameters");


        item.add(new SqlParam("string", rate.getCurrencyCouple()));
        item.add(new SqlParam("double", rate.getRate()));
        item.add(new SqlParam("timestamp", rate.getRateTS()));
        return item;
    }
}
