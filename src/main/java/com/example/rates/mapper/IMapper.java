package com.example.rates.mapper;

import com.example.rates.database.SqlParam;

import java.util.List;

public interface IMapper {
    List<List<SqlParam>> map(Object objClass);

    List<SqlParam> mapOne(Object objClass);
}
