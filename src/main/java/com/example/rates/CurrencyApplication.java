package com.example.rates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ConfigurationPropertiesScan
public class CurrencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrencyApplication.class, args);

//		ExecutorService executors = Executors.newFixedThreadPool(4);
//
//		// First thread: read data from source and store it at local buffer
//		executors.execute(new Consumer(inputQueue, flagInThread, instanceReturnable, sendId, parallelId, bulkSize));
//
//
//		// Second thread: read data from local buffer and store it at storage (kafka)
//		// Third thread: read data from storage (kafka) and store it at local buffer
//		// Fourth thread: read data from local buffer and send data to clients with web socket
//
//		executors.shutdown();
	}

}
