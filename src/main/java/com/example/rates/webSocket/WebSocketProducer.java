package com.example.rates.webSocket;

import com.example.rates.AProducer;
import com.example.rates.serde.IDeserializer;
import com.example.rates.service.IProducer;
import com.example.rates.service.RateSnapshotWrapper;
import com.example.rates.service.RatesService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Produce data from queue to Web Socket
 */
public class WebSocketProducer extends AProducer {

    @Autowired
    private RateSnapshotWrapper rateSnapshotWrapper;
    @Autowired
    private SimpMessagingTemplate template;

    public void produce(String message) {
        message = rateSnapshotWrapper.wrap(message);
        // Send rates to clients who listen this destination
        this.template.convertAndSend("/topic/rates", message);
    }

    public void produceFromBuffer() {
        // Wait for data in consumer queue
        String message = null;
        try {
            message = queue.poll(30, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        if (message != null) {
            // check message before send it
            if (check(message)) {
                produce(message);
            }
        }
    }


    @Override
    public void run() {

        while (true) {
            produceFromBuffer();
        }
    }
}
