package com.example.rates;

import com.example.rates.service.IConsumer;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.BlockingQueue;

@Slf4j
public abstract class AConsumer implements IConsumer {

    @Getter
    @Setter
    protected BlockingQueue<String> queue;

    @Override
    public void run() {

        log.info("Start consuming messages...");
        try {
            consume();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        log.info("Finished consuming messages...");
    }

    protected void addToQueue(String value) {
        int errorCount = 0;

        while (true) {
            try {
                queue.add(value);
                break;
            } catch (Exception e) {
                if (errorCount >= 10)
                    throw new RuntimeException("Error adding message to queue: " + e.getMessage());
                errorCount++;
                log.error("Error adding message to queue: " + e.getMessage());
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }

    }
}
