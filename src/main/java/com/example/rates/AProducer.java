package com.example.rates;

import com.example.rates.checker.IChecker;
import com.example.rates.model.RateDTO;
import com.example.rates.serde.IDeserializer;
import com.example.rates.service.IProducer;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

@Setter
@Getter
@Slf4j
public abstract class AProducer implements IProducer {

    IDeserializer deserializer;

    List<IChecker> checkChain;

    @Getter
    @Setter
    protected BlockingQueue<String> queue;

    @Override
    public void run() {
        try {
            produceFromQueue();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    void produceFromQueue() throws InterruptedException {
        String messages;
        while (true) {
            log.info("Waiting for data from queue...");
            // Wait for data in consumer queue
            messages = queue.poll(30, TimeUnit.SECONDS);

            if (messages != null) {
                produce(messages);
            }

        }
    }

    protected boolean check(String message) {
        for (IChecker checker : checkChain) {
            if (!checker.check(message, deserializer)) {
                return false;
            }
        }
        return true;
    }
}
