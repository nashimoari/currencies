package com.example.rates.service;

import com.example.rates.serde.IDeserializer;

import java.util.concurrent.BlockingQueue;

/**
 * Consume rates from source (kafka or some API for example, or it can be some fake generator)
 */
public interface IProducer extends Runnable{

    /**
     * Send received data
     */
    void produce(String message);

    void setQueue(BlockingQueue<String> queue);

    void setDeserializer(IDeserializer deserializer);

}
