package com.example.rates.service;
import java.util.concurrent.BlockingQueue;

/**
 * Consume rates from source (kafka or some API for example, or it can be some fake generator)
 */

public interface IConsumer extends Runnable {

    /**
     *
     * It works in infinitive loop. Receive data from source and store it in internal queue
     */
    void consume() throws InterruptedException;

    BlockingQueue<String> getQueue();

}
