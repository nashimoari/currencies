package com.example.rates.service;

import com.example.rates.model.RateDTO;

import java.util.Map;

/**
 * Generate new values for rates
 */
public class RatesGenerator {

    public void initRate(Map<String, RateDTO> rates, String currencyPair) {

        Double rate = switch (currencyPair) {
            case "EURUSD" -> 1.07;
            case "NZDUSD" -> 0.59;
            case "AUDUSD" -> 0.65;
            case "EURGBP" -> 0.86;
            case "GBPUSD" -> 1.25;
            default -> 1.0;
        };

        rates.put(currencyPair, new RateDTO(currencyPair, rate));
    }


    public void initRates(Map<String, RateDTO> rates, String[] currencyPairs) {

        for (String currencyPair : currencyPairs) {
            initRate(rates, currencyPair);

        }
    }

    /**
     * randomize rates
     */
    public void updateRates(Map<String, RateDTO> rates, String[] currencyPairs) {

        for (String currencyPair : currencyPairs) {
            if (!rates.containsKey(currencyPair)) {
                initRate(rates, currencyPair);
                continue;
            }

            // Get the current rate
            // Randomize the rate by a small amount
            // Update the rate in the rates map
            double rate = rates.get(currencyPair).getRate();
            rate += (Math.random() - 0.5) * 0.01;
            rates.get(currencyPair).setRate(rate);
            rates.get(currencyPair).setRateTS(System.currentTimeMillis());
        }

    }

}
