package com.example.rates.service;

import com.example.rates.model.RateDTO;
import com.example.rates.serde.JsonToRateDTODeserializer;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;

import java.sql.SQLException;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Produce rates to internal memory storage
 * Store rates for a certain time at the last 24 hours
 */

@Slf4j
public class RateSnapshotWrapper {

    RatesService ratesService;

    private final Map<String, RateDTO> rates = new HashMap<>();

    private final LocalTime startWindowTime;
    private final LocalTime endWindowTime;

    private final JsonToRateDTODeserializer deserializer;


    public RateSnapshotWrapper(String startWindowTime, String endWindowTime, RatesService ratesService) throws SQLException {
        this.startWindowTime = LocalTime.parse(startWindowTime);
        this.endWindowTime = LocalTime.parse(endWindowTime);
        this.deserializer = new JsonToRateDTODeserializer();

        List<RateDTO> rates = ratesService.getRatesOn5PMNY();
        for (RateDTO rate : rates) {
            this.rates.put(rate.getCurrencyCouple(), rate);
        }
        log.info("Wrapper initialized with rates: " + this.rates.toString());
    }


    public String wrap(String message) {
        RateDTO rateDTO = deserializer.deserialize(message);

        // check time
        ZonedDateTime rateTS = Instant.ofEpochMilli(rateDTO.getRateTS()).atZone(ZoneId.of("America/New_York"));

        LocalTime rateTime = rateTS.toLocalTime();

        if (rateTime.isAfter(startWindowTime) && rateTime.isBefore(endWindowTime)) {
            rates.put(rateDTO.getCurrencyCouple(), rateDTO);
        }

        if (rates.containsKey(rateDTO.getCurrencyCouple())) {
            rateDTO.setHistoryRateDiff(rateDTO.getRate() - rates.get(rateDTO.getCurrencyCouple()).getRate());
            rateDTO.setHistoryRateDiffTS(rates.get(rateDTO.getCurrencyCouple()).getRateTS());
        }

        message = new JSONObject(rateDTO).toString();
        log.info("wrapped message: " + message);

        return message;

    }

}
