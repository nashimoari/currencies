package com.example.rates.service;


import com.example.rates.AConsumer;
import com.example.rates.model.RateDTO;
import com.example.rates.model.Rates;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

@Slf4j
public class RatesConsumerFromGenerator extends AConsumer {

    @Getter
    private RatesGenerator ratesGenerator;

    @Getter
    int queueCapacity;

    @Getter
    private BlockingQueue<String> queue;

    String[] currencyPairs;

    Map<String, RateDTO> rateDTOMap = new HashMap<>();


    public RatesConsumerFromGenerator(int queueCapacity, String[] currencyPairs) {
        this.queueCapacity = queueCapacity;
        this.currencyPairs = currencyPairs;
        ratesGenerator = new RatesGenerator();
        queue = new ArrayBlockingQueue<>(queueCapacity);
        ratesGenerator.initRates(rateDTOMap, currencyPairs);
    }

    @Override
    public void consume() throws InterruptedException {

        while (true) {
            ratesGenerator.updateRates(rateDTOMap, currencyPairs);

            for (Map.Entry<String, RateDTO> entry : rateDTOMap.entrySet()) {
                queue.put(new JSONObject(entry.getValue()).toString());
            }

            log.info("Rates updated and added to queue");
            Thread.sleep(5000);
        }

    }

}
