package com.example.rates.service;

import com.example.rates.database.PostgresService;
import com.example.rates.model.RateDTO;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

@Service
public class RatesService {

    @Autowired
    PostgresService postgresService;

    private final String sqlGetRates = """
             with
             rate_5pm as (select t.ccy_couple, t.event_time, rate from (
             select ccy_couple, event_time, rate, row_number () OVER (PARTITION BY ccy_couple ORDER BY event_time desc) as row_num
             from public.rates_sample
             where event_time between
             date_trunc('day', now() at time zone 'GMT+4' - '17 hour '::interval, 'GMT+4')   + '17 hour '::interval - '30 sec'::interval
             and date_trunc('day', now() at time zone 'GMT+4' - '17 hour '::interval, 'GMT+4') + '17 hour '::interval) t
             where row_num = 1)
             select substring(t2.ccy_couple,1,3)||'/'||substring(t2.ccy_couple,4,3) as ccy_couple, t2.rate, EXTRACT(EPOCH FROM t2.event_time) as event_time, 
             t2.rate - r.rate as change
             from
             (select *
             from (
             select ccy_couple, event_time, rate, row_number () OVER (PARTITION BY ccy_couple ORDER BY event_time desc) as row_num
             from public.rates_sample rs
             where event_time > now() - '30 sec'::interval) t1
             where row_num = 1) t2
             left join rate_5pm r on r.ccy_couple = t2.ccy_couple
            """;

    private final String sqlGetRatesOn5PMNY = """
            select t.ccy_couple, EXTRACT(EPOCH FROM t.event_time) as event_time , rate from (
                     select ccy_couple, event_time, rate, row_number () OVER (PARTITION BY ccy_couple ORDER BY event_time desc) as row_num
                     from public.rates_sample
                     where event_time between
                     date_trunc('day', now() at time zone 'GMT+4' - '17 hour '::interval, 'GMT+4')   + '17 hour '::interval - '30 sec'::interval
                     and date_trunc('day', now() at time zone 'GMT+4' - '17 hour '::interval, 'GMT+4') + '17 hour '::interval) t
                     where row_num = 1
            """;

    public List<RateDTO> getRates() throws SQLException {
        ResultSet dbRates = postgresService.getData(sqlGetRates, null);

        List<RateDTO> result = new LinkedList<>();
        while (dbRates.next()) {
            RateDTO rateDTO = new RateDTO(dbRates.getString("ccy_couple"), dbRates.getDouble("rate"));
            rateDTO.setHistoryRateDiff(dbRates.getDouble("change"));
            rateDTO.setRateTS(dbRates.getLong("event_time"));
            result.add(rateDTO);
        }
        return result;
    }

    public List<RateDTO> getRatesOn5PMNY() throws SQLException {
        ResultSet dbRates = postgresService.getData(sqlGetRatesOn5PMNY, null);

        List<RateDTO> result = new LinkedList<>();
        while (dbRates.next()) {
            RateDTO rateDTO = new RateDTO(dbRates.getString("ccy_couple"), dbRates.getDouble("rate"));
            rateDTO.setRateTS(dbRates.getLong("event_time"));
            result.add(rateDTO);
        }
        return result;
    }

    public String convertRatesToJson(List<RateDTO> rates) {
        return new JSONArray(rates).toString();
    }

}
