package com.example.rates.service;

import com.example.rates.configuration.AppConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Receive data from a source and send it to storage
 */
@Slf4j
@Service
public class SourceHandler implements ApplicationRunner {

    @Autowired
    private AppConfiguration appConfiguration;

    IConsumer consumer;
    IProducer producer;

    public SourceHandler(AppConfiguration appConfiguration) throws UnknownHostException {
        this.consumer = appConfiguration.srcConsumer();
        this.producer = appConfiguration.srcProducer();
        this.producer.setQueue(consumer.getQueue());
    }

    /**
     * Receive data from source
     * @throws InterruptedException
     */
    void consumeRatesFromSource(ContextRefreshedEvent event) throws InterruptedException {
        log.info("Start listening for data from source...");
        consumer.consume();
    }

    /**
     * Send data to storage
     */
    void produceRatesFromSource() throws InterruptedException {
        BlockingQueue<String> queue = consumer.getQueue();

        while (true) {
            log.info("Waiting for data from source...");
            // Wait for data in consumer queue
            String rate = queue.poll(30, TimeUnit.SECONDS);

            if (rate != null) {
                producer.produce(rate);
            }

        }

    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        ExecutorService executors = Executors.newFixedThreadPool(2, new CustomizableThreadFactory("SourceHandler-"));


        // First thread: read data from source and store it at local buffer
        executors.execute(this.consumer);


        // Second thread: read data from local buffer and store it at storage (kafka)
        executors.execute(this.producer);

        executors.shutdown();
    }
}
