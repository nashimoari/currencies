package com.example.rates.service;

import com.example.rates.configuration.AppConfiguration;
import com.example.rates.model.Rates;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
@Service
public class WSRatesHandler implements ApplicationRunner {

    @Autowired
    private AppConfiguration appConfiguration;

    private IConsumer ratesConsumer;

    private IProducer ratesProducer;

    private Rates lastRates = new Rates();

    // Constructor

    public WSRatesHandler(AppConfiguration appConfiguration) {
        this.ratesConsumer = appConfiguration.ratesConsumerToClient();

        this.ratesProducer = appConfiguration.ratesProducer();
        this.ratesProducer.setQueue(ratesConsumer.getQueue());

    }


    void consumeRates() throws InterruptedException {
        ratesConsumer.consume();
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("WSRatesHandler started...");

        ExecutorService executors = Executors.newFixedThreadPool(2, new CustomizableThreadFactory("WSRatesHandler-"));


        // First thread: read data from source (kafka) and store it at local buffer
        executors.execute(this.ratesConsumer);


        // Second thread: read data from local buffer and produce it to clients
        executors.execute(this.ratesProducer);

        executors.shutdown();
    }
}
