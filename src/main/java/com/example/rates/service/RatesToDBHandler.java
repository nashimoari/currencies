package com.example.rates.service;

import com.example.rates.configuration.AppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
public class RatesToDBHandler implements ApplicationRunner {
    @Autowired
    private AppConfiguration appConfiguration;

    IConsumer consumer;

    IProducer producer;


    public RatesToDBHandler(AppConfiguration appConfiguration) throws UnknownHostException {
        this.consumer = appConfiguration.ratesConsumerToDB();
        this.producer = appConfiguration.ratesProduceToDB();
        this.producer.setQueue(consumer.getQueue());
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        ExecutorService executors = Executors.newFixedThreadPool(2, new CustomizableThreadFactory("RatesToDB-"));

        // First thread: read data from source and store it at local buffer
        executors.execute(this.consumer);


        // Second thread: read data from local buffer and store it at storage (DB)
        executors.execute(this.producer);

        executors.shutdown();
    }
}
