package com.example.rates.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service
public class Channels {

    // Subscribe/unsubscribe to/from channels for websocket sessions. Get subscribers for send next message to subscribers
    private Map<String, Set<String>> channels = new HashMap<>();
    private Map<String, String> sessions = new HashMap<>();

    public void subscribe(String channel, String sessionId) {
        //this.channels.getOrDefault(channel, new HashSet<>()).add(sessionId));
    }

    public void unsubscribe(String channel, String sessionId) {

    }

    public void getsubscribers(String channel) {

    }

    // Send message to subscribers

    public void send(String channel, String message) {

    }

    // Get all channels

    public void getchannels() {

    }


}
