package com.example.rates.serde;

import com.example.rates.model.RateDTO;
import com.google.gson.Gson;

public class JsonToRateDTODeserializer implements IDeserializer{
    public RateDTO deserialize(String json) {
        Gson gson = new Gson();
        RateDTO rate = gson.fromJson(json, RateDTO.class);
        return rate;
    }
}
