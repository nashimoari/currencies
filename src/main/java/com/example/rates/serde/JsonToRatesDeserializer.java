package com.example.rates.serde;

import com.example.rates.model.Rates;
import com.google.gson.Gson;

import java.util.Map;

public class JsonToRatesDeserializer implements IDeserializer{
    public Rates deserialize(String json) {
        Gson gson = new Gson();
        Map<String, Double> rates = gson.fromJson(json, Map.class);
        Rates ratesObj = new Rates();

        ratesObj.setRates(rates);

        return ratesObj;
    }
}
