package com.example.rates.serde;

public interface IDeserializer {
    Object deserialize(String json);
}
