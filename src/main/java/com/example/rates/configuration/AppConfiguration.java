package com.example.rates.configuration;

import com.example.rates.checker.IChecker;
import com.example.rates.checker.RateLastDateCheck;
import com.example.rates.database.DBProducer;
import com.example.rates.database.PostgresService;
import com.example.rates.kafka.MessageConsumer;
import com.example.rates.kafka.MessageProducer;
import com.example.rates.mapper.RateToSqlParam;
import com.example.rates.serde.JsonToRateDTODeserializer;
import com.example.rates.service.*;
import com.example.rates.webSocket.WebSocketProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.UnknownHostException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Configuration
public class AppConfiguration {

    @Autowired
    private RatesService ratesService;

    @Autowired
    private PostgresService postgresService;

    @Value("${Kafka.Bootstrap-server}")
    private String bootstrapServers;

    /**
     * 1 step: Describe source of rates
     *
     * @return
     */
    @Bean
    public IConsumer srcConsumer() {
        // Currency pairs that need to be obtained from the source
        String[] currencyPairs = new String[]{"EURUSD", "NZDUSD", "AUDUSD", "EURGBP", "GBPUSD"};

        RatesConsumerFromGenerator consumer = new RatesConsumerFromGenerator(100, currencyPairs);
        return consumer;
    }

    /**
     * 2 step: Describe how it stores rates received from source
     *
     * @return
     */
    @Bean
    public IProducer srcProducer() throws UnknownHostException {
        String topicName = "rates";
        String acks = "all";
        MessageProducer producer = new MessageProducer(bootstrapServers, topicName, acks);
        return producer;
    }

    /**
     * 3.1 step: Describe how it receives rates to store it at database
     *
     * @return
     */
    @Bean
    public IConsumer ratesConsumerToDB() {

        // Option 2: Receive rates from broker (kafka)
        String topicName = "rates";
        String groupId = "ratesGrpReaderToDB";
        String offsetResetConfig = "earliest";
        int queueCapacity = 100;
        return new MessageConsumer(bootstrapServers, topicName, groupId, offsetResetConfig, queueCapacity);
    }


    /**
     * 3.2 step: Describe how it receives rates to show it to users
     *
     * @return
     */
    @Bean
    public IConsumer ratesConsumerToClient() {

        // Option 1: Direct receive rates without broker
        // return new RatesConsumerFromGenerator();

        // Option 2: Receive rates from broker (kafka)
        String topicName = "rates";
        String groupId = "ratesGrpReaderToClient";
        String offsetResetConfig = "earliest";
        int queueCapacity = 100;
        return new MessageConsumer(bootstrapServers, topicName, groupId, offsetResetConfig, queueCapacity);
    }


    /**
     * 4.1 step: Describe how it sends data to database
     *
     * @return
     */
    @Bean
    public IProducer ratesProduceToDB() {
        String sql = "insert into public.rates_sample (ccy_couple, rate, event_time) values (?, ?, ?)";
        DBProducer dbProducer = new DBProducer();
        dbProducer.setSqlQuery(sql);
        dbProducer.setDataBase(postgresService);
        dbProducer.setDeserializer(new JsonToRateDTODeserializer());
        dbProducer.setMapper(new RateToSqlParam());
        return dbProducer;
    }


    /**
     * 4.2 step: Describe how it sends data to clients
     *
     * @return
     */
    @Bean
    public IProducer ratesProducer() {
        WebSocketProducer producer = new WebSocketProducer();
        producer.setDeserializer(new JsonToRateDTODeserializer());
        List<IChecker> checkers = new ArrayList<>();
        checkers.add(new RateLastDateCheck());
        producer.setCheckChain(checkers);
        return producer;
    }

    @Bean
    public RateSnapshotWrapper rateSnapshotWrapperBean() throws SQLException {
        return new RateSnapshotWrapper("16:59:29", "17:00:01", ratesService);
    }

}
