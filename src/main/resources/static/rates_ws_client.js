const stompClient = new StompJs.Client({
    brokerURL: 'ws://localhost:8080/rates'
});

rates = {}

stompClient.onConnect = (frame) => {
    setConnected(true);
    console.log('Connected: ' + frame);
    stompClient.subscribe('/topic/rates', (rate) => {
        updateRates(JSON.parse(rate.body));
        showRates(rates);
    });
};

stompClient.onWebSocketError = (error) => {
    console.error('Error with websocket', error);
};

stompClient.onStompError = (frame) => {
    console.error('Broker reported error: ' + frame.headers['message']);
    console.error('Additional details: ' + frame.body);
};

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#rates").show();
    }
    else {
        $("#rates").hide();
    }
    $("#ratesData").html("");
}

function connect() {
    stompClient.activate();
}

function disconnect() {
    stompClient.deactivate();
    setConnected(false);
    console.log("Disconnected");
}

function sendCommand() {
    stompClient.publish({
        destination: "/app/hello",
        body: JSON.stringify({'name': $("#name").val()})
    });
}

function updateRates(message) {
    rates[message.currencyCouple] = message;
}

function showRates(message) {
    html = ""
    for(const [pair, rate] of Object.entries(message)) {
        html +="<tr><td>" + pair + "</td><td>" + rate.rate + "</td><td>" + format_datetime(rate.rateTS/1000) + "</td>" +
        "<td>" + rate.historyRateDiff + "</td>" +
        "</tr>";
    }
    $("#ratesData").html(html);
    // $("#ratesData").append("<tr><td>" + message + "</td></tr>");
}


function format_datetime(s) {
  const dtFormat = new Intl.DateTimeFormat('en-GB', {
    timeStyle: 'medium',
    dateStyle: "short",
    timeZone: 'UTC'
  });

  return dtFormat.format(new Date(s * 1e3));
}

$(function () {
    $("form").on('submit', (e) => e.preventDefault());
    $( "#connect" ).click(() => connect());
    $( "#disconnect" ).click(() => disconnect());
    $( "#send" ).click(() => sendCommand());
});