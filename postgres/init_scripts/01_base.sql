create sequence public.rates_sample_seq;

-- A rates table with sample data for testing.
create table public.rates_sample (event_id bigint NOT NULL DEFAULT nextval('rates_sample_seq'::regclass),
event_time timestamptz NOT NULL , ccy_couple varchar(50) NOT NULL , rate float4 NOT NULL );

-- Generate data for last 2 days
INSERT INTO public.rates_sample
select
	nextval('public.rates_sample_seq'),
	to_timestamp(cast((extract(epoch from now() - '2 day'::interval) +  t.val1) as bigint))::timestamptz,
	case when val2 = 1 then 'EURUSD'
		when val2 = 2 then 'NZDUSD'
		when val2 = 3 then 'AUDUSD'
		when val2 = 4 then 'EURGBP'
		when val2 = 5 then 'GBPUSD'
	end,
	random() * 2
from
	(SELECT generate_series(1,172800) as val1, floor(random() * 5 + 1)::int as val2) t;